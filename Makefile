# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: garm <garm@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/09/23 12:05:07 by garm              #+#    #+#              #
##   Updated: 2015/02/19 04:41:15 by garm             ###   ########.fr       ##
#                                                                              #
# **************************************************************************** #

CC = gcc
ASM = nasm -f macho64

NAME = libfts.a
LIBS = -lfts

SOURCES_DIR = srcs
INCLUDES_DIR = .

ifeq ($(DEBUG), 1)
	FLAGS = -g -Wall -Wextra -pedantic
	CC = cc
else
	FLAGS = -Wall -Werror -Wextra
endif

CFLAGS = $(FLAGS) -L . $(LIBS) 

SOURCES = \
			 $(SOURCES_DIR)/ft_bzero.s \
			 $(SOURCES_DIR)/ft_strcat.s \
			 $(SOURCES_DIR)/ft_isalpha.s \
			 $(SOURCES_DIR)/ft_isdigit.s \
			 $(SOURCES_DIR)/ft_isalnum.s \
			 $(SOURCES_DIR)/ft_isascii.s \
			 $(SOURCES_DIR)/ft_isprint.s \
			 $(SOURCES_DIR)/ft_toupper.s \
			 $(SOURCES_DIR)/ft_tolower.s \
			 $(SOURCES_DIR)/ft_puts.s \
			 $(SOURCES_DIR)/ft_strlen.s \
			 $(SOURCES_DIR)/ft_memset.s \
			 $(SOURCES_DIR)/ft_memcpy.s \
			 $(SOURCES_DIR)/ft_strdup.s \
			 $(SOURCES_DIR)/ft_cat.s \
			 $(SOURCES_DIR)/ft_strchr.s \
			 $(SOURCES_DIR)/ft_strcmp.s \
			 $(SOURCES_DIR)/ft_strsub.s \
			 $(SOURCES_DIR)/ft_strjoin.s \
			 $(SOURCES_DIR)/ft_strcountc.s \

OBJS = $(SOURCES:.s=.o)

all: $(NAME)

%.o: %.s
	$(ASM) $< -o $@

$(NAME): $(OBJS)
	@echo Creating $(NAME)...
	@ar rcs $(NAME) $(OBJS)

clean:
	@rm -f $(OBJS)
	@echo Deleting $(NAME) OBJECTS files...

cleanbin:
	@rm -f $(NAME)
	@rm -f tests1
	@rm -f tests2
	@rm -f tests3
	@echo Deleting $(NAME)...

fclean: clean cleanbin
	@echo Full clean OK.

test: $(NAME)
	$(CC) $(CFLAGS) main.c -o tests1
	./tests1 && echo OK || echo ERROR

test2: $(NAME)
	$(CC) $(CFLAGS) main2.c -o tests2
	./tests2

test3: $(NAME)
	$(CC) $(CFLAGS) main3.c -o tests3
	./tests3

re: fclean all

.PHONY: clean cleanbin fclean re all test1 valgrind_test1 test2 valgrind_test2 test_all

