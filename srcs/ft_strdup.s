segment .text
global _ft_strdup ; const char *s1 (rdi)

extern _malloc
extern _ft_strlen
extern _ft_memcpy

_ft_strdup:
	push rdi		; save s1 pointer

	call _ft_strlen	; get s1 len + 1
	inc rax
	push rax

	mov rdi, rax ; call malloc with len
	call _malloc
	cmp rax, 0 ; if malloc return NULL
	je end ; return

	pop rdx			; set 3rd parameter of ft_memcpy to size.
	pop rsi			; set 2nd parameter of ft_memcpy to s1 pointer.
	mov rdi, rax	; set 1st parameter of ft_memcpy to new allocated pointer.
	call _ft_memcpy

ret

end:
	pop rdi
ret
