segment .text
global _ft_strjoin ; rdi (const char *s1), rsi (const char *s2)
extern _malloc
extern _ft_strcat
extern _ft_strlen

_ft_strjoin:
	push rsi
	push rdi
	call _ft_strlen
	push rax

	mov rdi, rsi
	call _ft_strlen

	mov rdi, rax
	pop rsi
	add rdi, rsi
	inc rdi
	call _malloc
	cmp rax, 0
	je END
	mov byte [rax], 0

	mov rdi, rax
	pop rsi
	call _ft_strcat

	mov rdi, rax
	pop rsi
	call _ft_strcat

END:
ret
