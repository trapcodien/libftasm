segment .text
global _ft_isdigit ; rdi (int c)

_ft_isdigit:
	mov rax, 1
	cmp rdi, 48 ; '0'
	jl fail
	cmp rdi, 57 ; '9'
	jg fail
	jmp end

fail:
	mov rax, 0
end:
ret
