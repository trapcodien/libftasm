segment .text

global _ft_strcat ; rdi (char *dst), rsi (const char *src)

_ft_strcat:
	mov rax, rdi

	; First Step : find \0 in s1
find:
	cmp byte [rdi], 0
	je copy
	inc rdi
	jmp find

	; Second Step : Copy bytes
copy:
	cmp byte [rsi], 0
	mov cl, byte [rsi]
	mov byte [rdi], cl
	je end
	inc rdi
	inc rsi
	jmp copy

end:
	ret
