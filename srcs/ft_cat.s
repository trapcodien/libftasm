%define BUFF_SIZE 4096

segment .text
global _ft_cat ; rdi (int fd)

_ft_cat:
	push rdi
LOOP:
	pop rdi
	lea rsi, [rel buffer]
	mov rdx, BUFF_SIZE
	mov rax, 0x2000003	; read syscall
	syscall
	jc END
	cmp rax, 0
	je END

	push rdi
	mov rdi, 1
	lea rsi, [rel buffer]
	mov rdx, rax
	mov rax, 0x2000004	; write syscall
	syscall
	jc POP_END
	cmp rax, 0
	je POP_END
	jmp LOOP

END:
ret

POP_END:
	pop rdi
ret

segment .data
buffer: times BUFF_SIZE db 0 

