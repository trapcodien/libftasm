segment .text
global _ft_strcountc ; rdi (const char *s), rsi (int c)

_ft_strcountc:
	xor rcx, rcx

LOOP:
	cmp byte [rdi], 0
	je END
	cmp byte [rdi], sil
	je INCREMENT_RCX
NEXT:
	inc rdi
jmp LOOP

INCREMENT_RCX:
	inc rcx
jmp NEXT

END:
	mov rax, rcx
ret

