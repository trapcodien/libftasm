segment .text
global _ft_isascii ; rdi (int c)

_ft_isascii:
	cmp rdi, 0
	jl exit_false
	cmp rdi, 127
	jg exit_false

	jmp exit_true

exit_true:
	mov rax, 1
ret

exit_false:
	mov rax, 0
ret
