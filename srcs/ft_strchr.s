segment .text
global _ft_strchr ; rdi (const char *s), rsi (int c)

_ft_strchr:
	cmp byte [rdi], sil		; sil is the 8 bits lower part of rsi
	je END
	cmp byte [rdi], 0
	je NOT_FOUND
	inc rdi
	jmp _ft_strchr

END:
	mov rax, rdi
ret

NOT_FOUND:
	mov rax, 0
ret
