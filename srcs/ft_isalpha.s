segment .text
global _ft_isalpha ; rdi (int c)

_ft_isalpha:
	cmp rdi, 65 ; 'A'
	jl exit_false
	cmp rdi, 122 ; 'z'
	jg exit_false
	cmp rdi, 90 ; 'Z'
	jg greater_eq_a
	jmp exit_true

greater_eq_a:
	cmp rdi, 97 ; 'a'
	jge exit_true
	jmp exit_false

exit_true:
	mov rax, 1
ret

exit_false:
	mov rax, 0
ret
