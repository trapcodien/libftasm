segment .text
global _ft_puts ; rdi (const char *s)

_ft_puts:
	cmp rdi, 0
	jne puts 
	lea rdi, [rel null]
	
puts:
	call strlen_rdi 		; param 3
	mov rsi, rdi			; param 2
	mov rdi, 1				; param 1
	mov     rax, 0x2000004	; write
	syscall
	jc exit_fail

	mov rdx, 1				; paran 3
	lea rsi, [rel nl]		; param 2
	mov     rax, 0x2000004 	; write
	syscall
	jc exit_fail

exit_ok:
	mov rax, 10
ret

exit_fail:
	mov rax, -1
ret


strlen_rdi: ; (size will be stored in rdx)
	push rdi
	mov rdx, 0
strlen_loop:
	cmp byte[rdi], 0
	je strlen_end
	inc rdi
	inc rdx
	jmp strlen_loop
strlen_end:
	pop rdi
ret


segment .data
nl:		db		10 ; '\n'
null:	db		"(null)"
