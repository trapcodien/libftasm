segment .text
global _ft_strsub ; rdi (const char *s), rsi (size_t start), rdx (size_t len)
extern _malloc
extern _ft_memcpy

_ft_strsub:
	push rdi
	push rsi
	push rdx

	mov rdi, rdx
	inc rdi
	call _malloc

	pop rdx
	pop rsi
	pop rdi

	cmp rax, 0
	je EXIT

	mov byte [rax + rdx], 0

	add rdi, rsi
	mov rsi, rdi
	mov rdi, rax
	call _ft_memcpy
ret


EXIT:
ret
