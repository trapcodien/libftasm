segment .text
global _ft_bzero ; rdi (void *s), rsi (size_t n)

_ft_bzero:
	cmp rsi, 0
	je end

	mov byte [rdi], 0

	inc rdi
	dec rsi

	jmp _ft_bzero

end:
	ret
