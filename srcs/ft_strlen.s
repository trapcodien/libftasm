segment .text
global _ft_strlen ; rdi (const char *s)

_ft_strlen:
	xor rax, rax ; set rax to 0
	xor rcx, rcx ; set rcx to 0

	not rcx 	; inverse byte
	cld			; left to right
	repne scasb ; while [rdi+rcx] not equal to rax (0)
	not rcx		; restore byte
	dec rcx

	mov rax, rcx

ret
