segment .text
global _ft_memset ; void *b (rdi) | int c (rsi) | size_t len (rdx)

_ft_memset:
	push rdi		; save b pointer

	mov rax, rsi	; copy c in rax (rax is used by stosb)
	mov rcx, rdx	; set rcx (used by rep)
	cld				; left to right iteration
	rep stosb		; rep decrement rcx until 0 (stosb : store rax to [rdi+rcx] )

	pop rax			; return b pointer
ret
