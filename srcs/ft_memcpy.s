segment .text
global _ft_memcpy ; void *dst (rdi) | const void *src (rsi) | size_t n (rdx)

_ft_memcpy:
	push rdi		; save dst pointer

	mov rcx, rdx	; set rcx (used by rep)
	cld				; left to right iteration
	rep movsb		; rep decrement rcx until 0 (movsb : copy [rsi+rcx] to [rdi+rcx])

	pop rax			; return dst pointer
ret
