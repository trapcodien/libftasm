segment .text
global _ft_strcmp ; rdi (const char *s1), rsi (const char *s2)

_ft_strcmp:
	xor rax, rax
	xor rcx, rcx
	mov cl, byte [rsi]
	cmp byte [rdi], cl
	jne DIFFERENT
	cmp byte [rdi], 0
	je END
	inc rdi
	inc rsi
	jmp _ft_strcmp

END:
	mov al, 0
ret

DIFFERENT:
	mov al, byte [rdi]
	mov cl, byte [rsi]
	sub eax, ecx
ret
