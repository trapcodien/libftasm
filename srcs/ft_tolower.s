segment .text
global _ft_tolower ; rdi (int c)

_ft_tolower:
	cmp rdi, 65 ; 'A'
	jl end
	cmp rdi, 90 ; 'Z'
	jg end
	
	add rdi, 32

end:
	mov rax, rdi
ret
