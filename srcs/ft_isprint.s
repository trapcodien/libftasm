segment .text
global _ft_isprint ; rdi (int c)

_ft_isprint:
	cmp rdi, 32
	jl exit_false
	cmp rdi, 126
	jg exit_false

	jmp exit_true

exit_true:
	mov rax, 1
ret

exit_false:
	mov rax, 0
ret
