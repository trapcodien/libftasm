segment .text
global _ft_toupper ; rdi (int c)

_ft_toupper:
	cmp rdi, 97 ; 'a'
	jl end
	cmp rdi, 122 ; 'z'
	jg end
	
	sub rdi, 32

end:
	mov rax, rdi
ret
