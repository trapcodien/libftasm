segment .text
global _ft_isalnum ; rdi (int c)

extern _ft_isalpha
extern _ft_isdigit

_ft_isalnum:
	call _ft_isalpha
	cmp rax, 1
	je exit_true

	call _ft_isdigit
	cmp rax, 1
	je exit_true
	
	jmp exit_false

exit_true:
	mov rax, 1
ret

exit_false:
	mov rax, 0
ret
