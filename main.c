#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <ctype.h>
#include <unistd.h>
#include <fcntl.h>

#define BUFF_SIZE	4096

#define RESET	"\033[0m"
#define RED		"\033[31m"
#define GREEN	"\033[32m"
#define YELLOW	"\033[33m"

/* PART 1 */
extern void ft_bzero(void *s, size_t n);
extern char *ft_strcat(char *s1, const char *s2);
extern int	ft_isalpha(int c);
extern int	ft_isdigit(int c);
extern int	ft_isalnum(int c);
extern int	ft_isascii(int c);
extern int	ft_isprint(int c);
extern int	ft_toupper(int c);
extern int	ft_tolower(int c);
extern int	ft_puts(const char *s);

/* PART 2 */
extern size_t ft_strlen(const char *s);
extern void * ft_memset(void *b, int c, size_t len);
extern void * ft_memcpy(void *dst, const void *src, size_t n);
extern char * ft_strdup(const char *s1);

/* PART 3 */
extern void		ft_cat(int fd);

/* PART 4 (BONUS) */
extern char *	ft_strchr(const char *s, int c);
extern int		ft_strcmp(const char *s1, const char *s2);
extern char *	ft_strsub(const char *s, size_t start, size_t len);
extern char *	ft_strjoin(const char *s1, const char *s2);
extern size_t	ft_strcountc(const char *s, int c);


int g_error = 0;

void	bzero_test(size_t size, char *s1, char *s2, size_t n)
{
	bzero(s1, n);
	ft_bzero(s2, n);

	if (memcmp(s1, s2, size))
	{
		printf("ft_bzero: " RED "[FAIL]" RESET "\n");
		g_error = 1;
		return ;
	}

	printf("ft_bzero: " GREEN "[OK]" RESET "\n");
}

void	strcat_test(size_t size, char *test1, char *test2, char *s2)
{
	test1 = strcat(test1, s2);
	test2 = ft_strcat(test2, s2);

	if (memcmp(test1, test2, size))
	{
		printf("ft_strcat: " RED "[FAIL]" RESET "\n");
		g_error = 1;
		return ;
	}

	printf("ft_strcat: " GREEN "[OK]" RESET "\n");
}

void	isalpha_test(void)
{
	int c = -4096;

	while (c <= 4096)
	{
		if (isalpha(c) != ft_isalpha(c))
		{
			printf("ft_isalpha: " RED "[FAIL]" RESET "\n");
			g_error = 1;
			return ;
		}
		c++;
	}
	printf("ft_isalpha: " GREEN "[OK]" RESET "\n");
}

void	isdigit_test(void)
{
	int c = -4096;

	while (c <= 4096)
	{
		if (isdigit(c) != ft_isdigit(c))
		{
			printf("ft_isdigit: " RED "[FAIL]" RESET "\n");
			g_error = 1;
			return ;
		}
		c++;
	}
	printf("ft_isdigit: " GREEN "[OK]" RESET "\n");
}

void	isalnum_test(void)
{
	int c = -4096;

	while (c <= 4096)
	{
		if (isalnum(c) != ft_isalnum(c))
		{
			printf("ft_isalnum: " RED "[FAIL]" RESET "\n");
			g_error = 1;
			return ;
		}
		c++;
	}
	printf("ft_isalnum: " GREEN "[OK]" RESET "\n");
}


void	isascii_test(void)
{
	int c = -4096;

	while (c <= 4096)
	{
		if (isascii(c) != ft_isascii(c))
		{
			printf("ft_isascii: " RED "[FAIL]" RESET "\n");
			g_error = 1;
			return ;
		}
		c++;
	}
	printf("ft_isascii: " GREEN "[OK]" RESET "\n");
}

void	isprint_test(void)
{
	int c = -4096;

	while (c <= 4096)
	{
		if (isprint(c) != ft_isprint(c))
		{
			printf("ft_isprint: " RED "[FAIL]" RESET "\n");
			g_error = 1;
			return ;
		}
		c++;
	}
	printf("ft_isprint: " GREEN "[OK]" RESET "\n");
}

void	toupper_test(void)
{
	int c = -4096;

	while (c <= 4096)
	{
		if (toupper(c) != ft_toupper(c))
		{
			printf("ft_toupper: " RED "[FAIL]" RESET "\n");
			g_error = 1;
			return ;
		}
		c++;
	}
	printf("ft_toupper: " GREEN "[OK]" RESET "\n");
}

void	tolower_test(void)
{
	int c = -4096;

	while (c <= 4096)
	{
		if (tolower(c) != ft_tolower(c))
		{
			printf("ft_tolower: " RED "[FAIL]" RESET "\n");
			g_error = 1;
			return ;
		}
		c++;
	}
	printf("ft_tolower: " GREEN "[OK]" RESET "\n");
}

int		puts_into_buffer(char const *str, char *buffer, int (*puts_func)(const char *))
{
	int		ret;
	int		stdout_save;
	int		tube[2];

	stdout_save = dup(1);

		/* DO WORKS */
	pipe(tube);
	dup2(tube[1], 1);
	ret = puts_func(str);
	read(tube[0], buffer, 1024);
	close(tube[0]);
	close(tube[1]);

		/* RESTORE STDOUT */
	dup2(stdout_save, 1);
	close(stdout_save);
	return ret;
}

int		puts_error(int (*puts_func)(const char *))
{
	int		ret;
	int		stdout_save;

	stdout_save = dup(1);

		/* CLOSE STDOUT FOR GENERATE AN ERROR */
	close(1);
	ret = puts_func(NULL);

		/* RESTORE STDOUT */
	dup2(stdout_save, 1);
	close(stdout_save);
	return ret;
}

void	puts_test(char const *str)
{
	char	buf1[1024];
	char	buf2[1024];
	int		ret1;
	int		ret2;

	bzero(buf1, 1024);
	bzero(buf2, 1024);

	ret1 = puts_into_buffer(str, buf1, &puts);
	ret2 = puts_into_buffer(str, buf2, &ft_puts);

	if (memcmp(buf1, buf2, 1024))
	{
		printf("ft_puts : " RED "ERROR (strings are different)" RESET "\n");
		g_error = 1;
		return ;
	}

	if (ret1 != ret2)
	{
		printf("ft_puts : " RED "ERROR (returns are different)" RESET "\n");
		g_error = 1;
		return ;
	}

	ret1 = puts_error(&puts);
	ret2 = puts_error(&ft_puts);
	if (ret1 != ret2)
	{
		printf("ft_puts : " RED "ERROR (returns are different)" RESET "\n");
		g_error = 1;
		return ;
	}

	printf("ft_puts: " GREEN "[OK]" RESET "\n");
}

void	strlen_test(const char *str)
{
	if (strlen(str) == ft_strlen(str))
		printf("ft_strlen: " GREEN "[OK]" RESET "\n");
	else
	{
		printf("ft_strlen: " RED "[FAIL]" RESET "\n");
		g_error = 1;
	}
}

void	memset_test(size_t size, char *s1, char *s2, size_t n, char c)
{
	memset(s1, c, n);
	ft_memset(s2, c, n);

	if (memcmp(s1, s2, size))
	{
		printf("ft_memset: " RED "[FAIL]" RESET "\n");
		g_error = 1;
		return ;
	}

	printf("ft_memset: " GREEN "[OK]" RESET "\n");
}

void	memcpy_test(size_t size, void *buf1, void *buf2, char *str, size_t n)
{
	buf1 = memcpy(buf1, str, n);
	buf2 = ft_memcpy(buf2, str, n);

	if (memcmp(buf1, buf2, size))
	{
		printf("ft_memcpy: " RED "[FAIL]" RESET "\n");
		g_error = 1;
		return ;
	}

	printf("ft_memcpy: " GREEN "[OK]" RESET "\n");
}

void	strdup_test(const char *s1)
{
	char *buf1;
	char *buf2;
	size_t size;

	size = strlen(s1) + 1;
	buf1 = ft_strdup(s1);
	buf2 = strdup(s1);

	if (memcmp(buf1, buf2, size))
	{
		printf("ft_strdup: " RED "[FAIL]" RESET "\n");
		g_error = 1;
	}
	else
		printf("ft_strdup: " GREEN "[OK]" RESET "\n");
	free(buf1);
	free(buf2);
}

void	_ft_cat(int fd)
{
	char	buf[BUFF_SIZE];
	ssize_t	rc;

	while ((rc = read(fd, buf, BUFF_SIZE)) > 0)
	{
		if (write(1, buf, rc) <= 0)
			return ;
	}
}

void	cat_into_buffer(int fd, char *buf, void (*cat_func)(int))
{
	int	stdout_save;
	int	tube[2];

	stdout_save = dup(1);
	pipe(tube);
	dup2(tube[1], 1);
	cat_func(fd);
	read(tube[0], buf, 10000);
	close(tube[0]);
	close(tube[1]);
	dup2(stdout_save, 1);
	close(stdout_save);
}

void	cat_test(void)
{
	char	buf[10000];
	int		fd = open("/dev/random", O_RDONLY);
	int		tube1[2];
	int		tube2[2];
	char	buf1[10000];
	char	buf2[10000];

	read(fd, buf, 10000); /* Read 10k random chars */
	close(fd);

	pipe(tube1);
	write(tube1[1], buf, 10000);
	close(tube1[1]);

	pipe(tube2);
	write(tube2[1], buf, 10000);
	close(tube2[1]);

	cat_into_buffer(tube1[0], buf1, &_ft_cat);
	close(tube1[0]);

	cat_into_buffer(tube2[0], buf2, &ft_cat);
	close(tube2[0]);

	if (memcmp(buf1, buf2, 10000))
	{
		g_error = 1;
		printf("ft_cat: " RED "[FAIL]" RESET "\n");
	}
	else
		printf("ft_cat: " GREEN "[OK]" RESET "\n");
}

void	strchr_test(const char *s, int c)
{
	if (ft_strchr(s, c) == strchr(s, c))
		printf("ft_strchr: " GREEN "[OK]" RESET "\n");
	else
	{
		g_error = 1;
		printf("ft_strchr: " RED "[FAIL]" RESET "\n");
	}
}

void	strcmp_test(const char *s1, const char *s2)
{
	int		ret1;
	int		ret2;

	ret1 = strcmp(s1, s2);
	if (ret1 < 0)
		ret1 = -1;
	else if (ret1 > 0)
		ret1 = 1;

	ret2 = ft_strcmp(s1, s2);
	if (ret2 < 0)
		ret2 = -1;
	else if (ret2 > 0)
		ret2 = 1;

	if (ret1 == ret2)
		printf("ft_strcmp: " GREEN "[OK]" RESET "\n");
	else
	{
		g_error = 1;
		printf("ft_strcmp: " RED "[FAIL]" RESET "\n");
	}
}

void	strsub_test(const char *s, size_t start, size_t len, const char *result)
{
	char *new_string;

	new_string = ft_strsub(s, start, len);
	if (!strcmp(new_string, result))
		printf("ft_strsub: " GREEN "[OK]" RESET "\n");
	else
	{
		g_error = 1;
		printf("ft_strsub: " RED "[FAIL]" RESET "\n");
	}
	free(new_string);
}

void	strjoin_test(const char *s1, const char *s2, const char *result)
{
	char *new_string;

	new_string = ft_strjoin(s1, s2);
	if (!strcmp(new_string, result))
		printf("ft_strjoin: " GREEN "[OK]" RESET "\n");
	else
	{
		g_error = 1;
		printf("ft_strjoin: " RED "[FAIL]" RESET "\n");
	}
	free(new_string);
}

void	strcountc_test(const char *s, int c, size_t result)
{
	if (ft_strcountc(s, c) == result)
		printf("ft_strcountc: " GREEN "[OK]" RESET "\n");
	else
	{
		g_error = 1;
		printf("ft_strcountc: " RED "[FAIL]" RESET "\n");
	}
}

int		main(void)
{
	char *str1;
	char *str2;
	size_t size;
/***************** FT_BZERO ***********************/
	str1 = strdup("Hello World !");
	str2 = strdup("Hello World !");
	size = strlen(str1) + 1;
	fprintf(stderr, YELLOW "1 : " RESET); bzero_test(size, str1, str2, 0);
	fprintf(stderr, YELLOW "2 : " RESET); bzero_test(size, str1, str2, 1);
	fprintf(stderr, YELLOW "3 : " RESET); bzero_test(size, str1, str2, size);
	free(str1);
	free(str2);
	printf("\n");

/***************** FT_STRCAT **********************/
	str1 = malloc(15); bzero(str1, 15);
	str2 = malloc(15); bzero(str2, 15);
	fprintf(stderr, YELLOW "4 :" RESET); strcat_test(15, str1, str2, "Hello");
	fprintf(stderr, YELLOW "5 :" RESET); strcat_test(15, str1, str2, " ");
	fprintf(stderr, YELLOW "6 :" RESET); strcat_test(15, str1, str2, "World");
	fprintf(stderr, YELLOW "7 :" RESET); strcat_test(15, str1, str2, "!");
	free(str1);
	free(str2);
	printf("\n");

/***************** FT_ISALPHA *********************/
	fprintf(stderr, YELLOW "8: " RESET); isalpha_test();
	printf("\n");

/***************** FT_ISDIGIT *********************/
	fprintf(stderr, YELLOW "9: " RESET); isdigit_test();
	printf("\n");

/***************** FT_ISALNUM *********************/
	fprintf(stderr, YELLOW "10: " RESET); isalnum_test();
	printf("\n");

/***************** FT_ISASCII *********************/
	fprintf(stderr, YELLOW "11: " RESET); isascii_test();
	printf("\n");

/***************** FT_ISPRINT *********************/
	fprintf(stderr, YELLOW "12: " RESET); isprint_test();
	printf("\n");

/***************** FT_TOUPPER *********************/
	fprintf(stderr, YELLOW "13: " RESET); toupper_test();
	printf("\n");

/***************** FT_TOLOWER *********************/
	fprintf(stderr, YELLOW "14: " RESET); tolower_test();
	printf("\n");

/***************** FT_PUTS ************************/
	fprintf(stderr, YELLOW "15: " RESET); puts_test(NULL);
	fprintf(stderr, YELLOW "16: " RESET); puts_test("");
	fprintf(stderr, YELLOW "17: " RESET); puts_test("Hello World !");
	fprintf(stderr, YELLOW "18: " RESET); puts_test("QWERTYUIOPASDFGHJKLZXCVBNM\n\n\n\n");
	printf("\n");

/***************** FT_STRLEN **********************/
	fprintf(stderr, YELLOW "20: " RESET); strlen_test("");
	fprintf(stderr, YELLOW "21: " RESET); strlen_test("Hello World !");
	fprintf(stderr, YELLOW "22: " RESET); strlen_test("QWERTYUIOPASDFGHJKLZXCVBNM\n\n\n\n");
	printf("\n");

/***************** FT_MEMSET **********************/
	str1 = strdup("Hello World !");
	str2 = strdup("Hello World !");
	size = strlen(str1);
	fprintf(stderr, YELLOW "23 : " RESET); memset_test(size, str1, str2, 0, 0);
	fprintf(stderr, YELLOW "24 : " RESET); memset_test(size, str1, str2, 1, 'X');
	fprintf(stderr, YELLOW "25 : " RESET); memset_test(size, str1, str2, size, '?');
	free(str1);
	free(str2);
	printf("\n");

/***************** FT_MEMCPY **********************/
	str1 = malloc(15); bzero(str1, 15);
	str2 = malloc(15); bzero(str2, 15);
	fprintf(stderr, YELLOW "26 :" RESET); memcpy_test(15, str1, str2, "Hello", 5);
	fprintf(stderr, YELLOW "27 :" RESET); memcpy_test(15, str1, str2, " ", 1);
	fprintf(stderr, YELLOW "28 :" RESET); memcpy_test(15, str1, str2, "World", 3);
	fprintf(stderr, YELLOW "29 :" RESET); memcpy_test(15, str1, str2, "Hello World !", 13);
	free(str1);
	free(str2);

	printf("\n");

/***************** FT_STRDUP **********************/
	fprintf(stderr, YELLOW "30 :" RESET); strdup_test("");
	fprintf(stderr, YELLOW "31 :" RESET); strdup_test("Hello");
	fprintf(stderr, YELLOW "32 :" RESET); strdup_test("Hello World !");
	printf("\n");

/***************** FT_CAT *************************/
	fprintf(stderr, YELLOW "33 :" RESET); cat_test();
	printf("\n");

/***************** FT_STRCHR **********************/
	fprintf(stderr, YELLOW "34 :" RESET); strchr_test("", '\0');
	fprintf(stderr, YELLOW "35 :" RESET); strchr_test("", 'z');
	fprintf(stderr, YELLOW "36 :" RESET); strchr_test("yolo", '\0');
	fprintf(stderr, YELLOW "37 :" RESET); strchr_test("yolo", 'o');
	fprintf(stderr, YELLOW "38 :" RESET); strchr_test("yolo", 'l');
	fprintf(stderr, YELLOW "39 :" RESET); strchr_test("yolo", 'z');
	printf("\n");

/***************** FT_STRCMP **********************/	
	fprintf(stderr, YELLOW "40 :" RESET); strcmp_test("", "");
	fprintf(stderr, YELLOW "41 :" RESET); strcmp_test("yolo", "yolo");
	fprintf(stderr, YELLOW "42 :" RESET); strcmp_test("yolo", "yolo123");
	fprintf(stderr, YELLOW "43 :" RESET); strcmp_test("yolo123", "yolo");
	fprintf(stderr, YELLOW "44 :" RESET); strcmp_test("yolo", "LOLILOL");
	fprintf(stderr, YELLOW "44 :" RESET); strcmp_test("LOLILOL", "yolo");
	fprintf(stderr, YELLOW "45 :" RESET); strcmp_test("yolo", "");
	printf("\n");

/***************** FT_STRSUB **********************/	
	fprintf(stderr, YELLOW "46 :" RESET); strsub_test("", 0, 0, "");
	fprintf(stderr, YELLOW "47 :" RESET); strsub_test("lol", 0, 0, "");
	fprintf(stderr, YELLOW "48 :" RESET); strsub_test("lol", 1, 0, "");
	fprintf(stderr, YELLOW "49 :" RESET); strsub_test("lol", 2, 0, "");
	fprintf(stderr, YELLOW "50 :" RESET); strsub_test("lol", 0, 1, "l");
	fprintf(stderr, YELLOW "51 :" RESET); strsub_test("lol", 0, 2, "lo");
	fprintf(stderr, YELLOW "52 :" RESET); strsub_test("lol", 0, 3, "lol");
	fprintf(stderr, YELLOW "53 :" RESET); strsub_test("lol", 1, 2, "ol");
	fprintf(stderr, YELLOW "54 :" RESET); strsub_test("lol", 1, 1, "o");
	fprintf(stderr, YELLOW "55 :" RESET); strsub_test("lol", 2, 1, "l");
	printf("\n");

/***************** FT_STRJOIN *********************/	
	fprintf(stderr, YELLOW "56 :" RESET); strjoin_test("", "", "");
	fprintf(stderr, YELLOW "57 :" RESET); strjoin_test("yolo", "", "yolo");
	fprintf(stderr, YELLOW "58 :" RESET); strjoin_test("", "yolo", "yolo");
	fprintf(stderr, YELLOW "59 :" RESET); strjoin_test("yo", "lo", "yolo");
	fprintf(stderr, YELLOW "60 :" RESET); strjoin_test("Hello ", "World !", "Hello World !");
	printf("\n");

/***************** FT_STRCOUNTC *******************/	
	fprintf(stderr, YELLOW "61 :" RESET); strcountc_test("", '\0', 0);
	fprintf(stderr, YELLOW "62 :" RESET); strcountc_test("yolo", '\0', 0);
	fprintf(stderr, YELLOW "63 :" RESET); strcountc_test("yolo", 'y', 1);
	fprintf(stderr, YELLOW "64 :" RESET); strcountc_test("yolo", 'o', 2);
	fprintf(stderr, YELLOW "65 :" RESET); strcountc_test("Hello World !", 'l', 3);
	fprintf(stderr, YELLOW "66 :" RESET); strcountc_test("Hello World !", 'z', 0);
	fprintf(stderr, YELLOW "67 :" RESET); strcountc_test("Hello World !", '!', 1);

/**************************************************/;
	return g_error;
}
